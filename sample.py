from sklearn.datasets import load_iris
from sklearn import tree
import matplotlib.pyplot as plt

X = [[0, 0], [1, 1], [1, 0], [0, 1], [2, 1], [1, 0], [2, 0], [2, 1], [0, 0], [2, 0], [1, 0], [1, 1], [2, 0]]
Y = [1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0]
clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, Y)

print(clf.predict([[1, 1], [2, 1], [2, 0], [1, 0], [0, 1]]))